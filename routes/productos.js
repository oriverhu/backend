const express = require('express');
const Product = require('./../models/producto');
const app = express();

app.post('/getProductsByParam', (req, res) => {
    let body = req.body;
    let param = body.param;

    if(!param){
        res.status(409).json({
            ok:false,
            error: 'No se recibió el parámetro de búsqueda'
        })
    }else{

        var search = {}
        if(param.length<=3){
            search = { id:  param }
        }else{
           search = {
                $or: [
                    {'brand': new RegExp(param, 'i')},
                    {'description': new RegExp(param, 'i')}
                ]
            }
        }

        Product.find(search)
            .exec((error, datos) => {
                if (error) {
                    res.status(400).json({
                        ok: false,
                        error
                    });
                    return
                }

                var result = []
                var validator = param.length >3 ? palindromo(param) : false;
                if(datos.length>0){
                    for(var i=0;i<datos.length;i++){
                        datos[i].discount = validator ? 50 : 0 
                        datos[i].price = validator ? datos[i].price * 0.50 : datos[i].price 
                        result.push(datos[i])
                    }
                }
            
                res.json({
                    ok:true,
                    palindromo: validator ? 'si' : 'no',
                    datos: result,              
                })
            })

    }
  
})


function palindromo(text)
{
	// eliminamos los espacios en blanco
    var palabra=text.replace(/ /g, "");  
	for (var i=0;i<palabra.length;i++){
		if(palabra[i]!=palabra[palabra.length-i-1]){
			return false;
		}
	}
	return true;
}

app.get('/getProductsById/:id', (req, res) => {
    let id = req.params.id;

    Product.findById(id, (err, datos) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                mensaje: err
            });
        }

        res.json({
            ok: true,
            datos,
        });
    })
})

module.exports = app;