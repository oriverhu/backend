const mongoose = require('mongoose')
const Schema = mongoose.Schema;

let productSchema = new Schema({    
    id: {
        type: Number,
    },
    brand: {
        type: String,
    },
    description: {
        type: String,
    },
    image: {
        type: String,
    },
    price: { 
        type: Number, 
    },   
    discount: {
        type: Number,
    },
});

module.exports = mongoose.model('Product', productSchema);
