require('./config/config')

const mongoose = require('mongoose');
mongoose.connect(process.env.urlBD, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}, (err, resp) => {
    if (err) throw console.log("ERROR BD: " + err);
    console.log("Base de datos");
});

const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(require('./routes'))

app.listen(process.env.PORT, () => {
    console.log("Escuchando puerto " + process.env.PORT + ' \nEn: ' + process.env.urlBD);
})