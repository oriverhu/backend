//================================== 
//   Puerto
//================================== 

process.env.PORT = process.env.PORT || 4000;

//================================== 
//   Entorno
//================================== 
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

//================================== 
//   Base de datos
//================================== 

let urlBD

if (process.env.NODE_ENV === 'dev') {
    urlBD = 'mongodb://localhost:27017/wallmart';
} 
process.env.urlBD = urlBD;
